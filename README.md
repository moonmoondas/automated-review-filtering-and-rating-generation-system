# Automated Review Filtering and Rating Generation System



## About

This Project filter out spam reviews and generate rating of a product in an e-commerce website by analyzing the text based review provided by the users using deep learning.The system uses sequence learning methodology in order to achieve desired functionality. The reviews provided by the users about a product on an e-commerce website is analyzed and rating of that product is generated based on the review. This system leverages the power of machine learning to completely eradicates the trouble of giving rating as well as writing review and helps to predict accurate rating based on user reviews. 
I have built a pipeline that takes a collection of reviews as input and provide product ratings as an output. In the meantime, from the input, the irrelevant and spam instances are filtered out using a classifier model. From this pre-processed input, product rating is generated using machine learning model. By using this system, an e-commerce company can know about their product inventory,customer base and this will help them for product marketing and promotions.


## Installation

The application runs on Python's Flask micro web framework. The following packages are required to be installed in order to run this application.

    Packages for Sentiment Analysis
     
    numpy
    pandas
    tqdm
    tensorflow
    sklearn
    matplotlib
    keras
    nltk



    Packages for Web Application

    flask
    SQLAlchemy
    Flask-MySQL
    mysqlclient

